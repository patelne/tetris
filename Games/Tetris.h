#pragma once
#include <functional>
#include <memory>
#include <time.h>
#include <vector>

#include "BaseGame.h"
#include "TetrisConstants.h"
#include "TetrisPiece.h"
#include "TetrisSquare.h"

class Tetris : public BaseGame {
public:
    Tetris() : Tetris(W_BOARD, H_BOARD) {};
    Tetris(int,int);

    virtual int init(SDL_Window*&, SDL_Renderer*&, SDL_Thread*&);

    virtual int render(SDL_Renderer*);
    virtual int timer();

    virtual void process_event(SDL_Event*);

private:
    unsigned long score;

    std::vector< std::vector< std::shared_ptr<TetrisSquare> > > board;
    std::unique_ptr<TetrisPiece> cur_piece;

    int Tetris::set_new_cur_piece();
};

