#include "TetrisPiece.h"

TetrisPiece::TetrisPiece(piece_shape shape, int x_range, int y) {
    this->shape = shape;
    int x = rand() % x_range;

    switch(shape) {
        case(SHORT_T_SHAPE):
            x = std::min(x, x_range-1);
            x = std::max(x, 1);

            for(size_t i = 0; i < 2; i++ ) {
                squares.push_front(std::make_shared<TetrisSquare>(shape, x, y+i));
            }
            squares.push_front(std::make_shared<TetrisSquare>(shape, x-1, y));
            squares.push_front(std::make_shared<TetrisSquare>(shape, x+1, y));

            break;

        case(LONG_T_SHAPE):
            x = std::min(x, x_range-1);
            x = std::max(x, 1);

            for(size_t i = 0; i < 3; i++ ) {
                squares.push_front(std::make_shared<TetrisSquare>(shape, x, y+i));
            }
            squares.push_front(std::make_shared<TetrisSquare>(shape, x-1, y));
            squares.push_front(std::make_shared<TetrisSquare>(shape, x+1, y));
            break;

        case(L_SHAPE):
            x = std::min(x, x_range-1);

            for(size_t i = 0; i < 3; i++ ) {
                squares.push_front(std::make_shared<TetrisSquare>(shape, x, y+i));
            }
            squares.push_front(std::make_shared<TetrisSquare>(shape, x+1, y+2));

            break;

        case(J_SHAPE):
            x = std::max(x, 1);

            for(size_t i = 0; i < 3; i++ ) {
                squares.push_front(std::make_shared<TetrisSquare>(shape, x, y+i));
            }
            squares.push_front(std::make_shared<TetrisSquare>(shape, x-1, y+2));

            break;

        case(I_SHAPE):
            for(size_t i = 0; i < 4; i++ ) {
                squares.push_front(std::make_shared<TetrisSquare>(shape, x, y+i));
            }

            break;

        default:
            break;
    }
}

int TetrisPiece::render(SDL_Renderer* renderer) {
    for( auto it = squares.begin(); it != squares.end(); it++ ) {
        (it->get())->render(renderer);
    }

    return 0;
}

void TetrisPiece::shift_down() {
    for( auto it = squares.begin(); it != squares.end(); it++ ) {
        (it->get())->shift_down();
    }
}