#include "Tetris.h"

Tetris::Tetris(int width, int height) {
    srand(static_cast<unsigned int>(time(NULL)));

    board.resize(width);
    for (size_t i = 0; i < board.size(); i++) {
        board[i].reserve(height);
        for(int j = 0; j < height; j++) {
            board[i].push_back( std::make_shared<TetrisSquare>(i,j) );
        }
    }

    cur_piece = nullptr;
    set_new_cur_piece();
}

int Tetris::init(SDL_Window*& win, SDL_Renderer*& renderer, SDL_Thread*& logic_thread) {
    win = SDL_CreateWindow("Tetris", X_WIN, Y_WIN, W_WIN, H_WIN, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);

    return (!win || !renderer) ? 1 : 0;
}

int Tetris::render(SDL_Renderer* renderer) {
    /*Render the board*/
    for (size_t i= 0; i < board.size(); i++) {
        for (size_t j = 0; j < board[i].size(); j++) {
            board[i][j]->render(renderer);
        }
    }
    
    /*Render the current piece*/
    cur_piece->render(renderer);
    return 0;
}

int Tetris::timer() {
    bool load_new_piece = false;
    auto square_list = cur_piece->get_squares();

    for( auto it = square_list->begin(); it != square_list->end(); it++ ) {
        std::pair<int, int> pos = it->get()->get_pos();

        /*A square cannot go any lower*/
        if(pos.second == H_BOARD - 1 || board[pos.first][pos.second+1]->get_color() != COLOR_INV) {
            load_new_piece = true;
        }
    }
    
    if( load_new_piece ) {
        /*Store the squares and load a new piece*/
        for( auto it = square_list->begin(); it != square_list->end(); it++ ) {
            std::pair<int, int> pos = it->get()->get_pos();
            board[pos.first][pos.second] = *it;
        }
        set_new_cur_piece();
    } else {
        cur_piece->shift_down();
    }

    return 0;
}

int Tetris::set_new_cur_piece() {
    //Create a piece with a random shape and random x position at the top
    piece_shape shape = static_cast<piece_shape>(rand()%piece_shape::SHAPE_CNT);
    int x_range = board.size() - 1;
    int y = 0;

    cur_piece = std::make_unique<TetrisPiece>(shape, x_range, y);

    return 0;
}

void Tetris::process_event(SDL_Event* e) {
    switch (e->type) {
        case (SDL_KEYDOWN):
            switch(e->key.keysym.sym) {
                case(SDLK_DOWN):
                case(SDLK_RIGHT):
                case(SDLK_LEFT):
                    break;

                case(SDLK_UP):
                    /*Shouldn't do anything*/
                default:
                    break;
            }

            break;

        default:
            break;
    }
}