#include <iostream>
#include <memory>
#include <queue>
#include <SDL.h>
#include <SDL_thread.h>

#include "BaseGame.h"
#include "Tetris.h"

const int FPS = 60;

Uint32 send_timer(Uint32, void*);

int main(int argc, char**argv){
    SDL_Window   *win       = nullptr;
    SDL_Renderer *renderer  = nullptr;
    SDL_Thread   *logic_thread = nullptr;
    SDL_TimerID  timer_id;

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
        std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
        return 1;
    }

    //Initialize and load game
    std::unique_ptr<BaseGame> b_game(new Tetris);
    if (b_game->init(win, renderer, logic_thread)) {
        return 1;
    }
    
    timer_id = SDL_AddTimer(1000/FPS, send_timer, b_game.get());

    SDL_Event e;
    bool quit = false;
    Uint32 frame_timer = 0;
    while (!quit) {
        while (SDL_PollEvent(&e)) {
            switch (e.type) {
                case SDL_QUIT:
                    quit = true;
                    break;

                default:
                    b_game->process_event(&e);
                    break;
            }
        }
        
        if( SDL_TICKS_PASSED(SDL_GetTicks(), frame_timer) ) {
            frame_timer = SDL_GetTicks() + (1000 / FPS);

            SDL_RenderClear(renderer);
            b_game->render(renderer);
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            SDL_RenderPresent(renderer);
        }
    }

    SDL_Quit();
    return 0;
}

Uint32 send_timer(Uint32 interval, void* params) {
    BaseGame* b_game = (BaseGame*)params;

    b_game->timer();
    
    return interval;
}