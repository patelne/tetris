#include "BaseGame.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

BaseGame::BaseGame() {
}

BaseGame::~BaseGame() {
}

int BaseGame::init(SDL_Window* &win, SDL_Renderer* &renderer, SDL_Thread* &logic_thread) {
    win = SDL_CreateWindow("Hello World!", 100, 100, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

    return (!win || !renderer) ? 1 : 0;
}

int BaseGame::render(SDL_Renderer* renderer) {
    return 0;
}

int BaseGame::timer() {
    return 0;
}
 
void BaseGame::process_event(SDL_Event* e) {
    /*Process Menu handling*/
}