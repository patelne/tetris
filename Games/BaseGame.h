#pragma once
#include <SDL.h>
#include <cstdlib>

class BaseGame
{
public:
    BaseGame();
    ~BaseGame();

    virtual int init(SDL_Window*&, SDL_Renderer*&, SDL_Thread*&);
    virtual int render(SDL_Renderer*);
    virtual void process_event(SDL_Event*);
    virtual int timer();
};
