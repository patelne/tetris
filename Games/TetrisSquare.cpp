#include "TetrisSquare.h"

int TetrisSquare::render(SDL_Renderer* renderer) {
    if (color < COLOR_INV) {
        SDL_Rect rect;
        
        rect.x = x * sq_width;
        rect.y = y * sq_height;
        rect.w = sq_width;
        rect.h = sq_height;

        SDL_SetRenderDrawColor(renderer, clr_tbl_r[color], clr_tbl_g[color],
                                         clr_tbl_b[color], clr_tbl_a[color]);
        SDL_RenderFillRect(renderer, &rect);
    }

    return 0;
}
