#pragma once
#include <algorithm>
#include <functional>
#include <forward_list>
#include <memory>
#include <SDL.h>

#include "TetrisConstants.h"
#include "TetrisSquare.h"

class TetrisPiece {
public:
    TetrisPiece(piece_shape, int x, int y);
    
    int render(SDL_Renderer*);
    
    void shift_down();

    std::forward_list<std::shared_ptr<TetrisSquare> >* get_squares() {return &squares; }

private:
    piece_shape shape;
    std::forward_list<std::shared_ptr<TetrisSquare> > squares;
};
