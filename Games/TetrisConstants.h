#ifndef TETRIS_CONSTANTS_H
#define TETRIS_CONSTANTS_H

#define W_WIN 600
#define H_WIN 1200
#define X_WIN 100
#define Y_WIN 100

#define W_BOARD 15
#define H_BOARD 40

enum square_color{
    RED,
    GREEN,
    BLUE,
    YELLOW,
    CYAN,

    COLOR_INV,
    COLOR_CNT = COLOR_INV
};

enum piece_shape{
    SHORT_T_SHAPE,
    LONG_T_SHAPE,
    L_SHAPE,
    J_SHAPE,
    I_SHAPE,

    SHAPE_INV,
    SHAPE_CNT = SHAPE_INV
};

static_assert(COLOR_CNT == SHAPE_CNT, "Tetris Piece Enum counts are different");

                                     //RED  GREEN   BLUE    YELLOW  CYAN  
const Uint8 clr_tbl_r[COLOR_CNT] = { 0xFF,  0,      0,      0xFF,   0x0    };
const Uint8 clr_tbl_g[COLOR_CNT] = { 0,     0xFF,   0,      0xFF,   0xFF    };
const Uint8 clr_tbl_b[COLOR_CNT] = { 0,     0,      0xFF,   0,      0xFF    };
const Uint8 clr_tbl_a[COLOR_CNT] = { 0xFF,  0xFF,   0xFF,   0xFF,   0xFF    };

#endif