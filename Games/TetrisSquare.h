#pragma once
#include <SDL.h>
#include <utility>

#include "TetrisConstants.h"

/*Equally distribut squares across the board*/
static const int sq_width   = H_WIN / H_BOARD;
static const int sq_height  = sq_width;

class TetrisSquare
{
public:
    TetrisSquare() : TetrisSquare(0,0) {};
    TetrisSquare(int in_x, int in_y) : TetrisSquare( SHAPE_INV, in_x, in_y) {};
    TetrisSquare(piece_shape in_color, int in_x, int in_y) : color((square_color)in_color), x(in_x), y(in_y) {};

    int render(SDL_Renderer*);

    void shift_down() { y+=1; }

    void set_pos(int x, int y) {this->x = x; this->y = y;}
    std::pair<int,int> get_pos() {return std::make_pair(x,y);}
    square_color get_color() { return color; }

private:
    int x, y;
    square_color color;
};
